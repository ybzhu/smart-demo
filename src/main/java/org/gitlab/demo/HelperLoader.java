package org.gitlab.demo;

import org.gitlab.demo.helper.BeanHelper;
import org.gitlab.demo.helper.ClassHelper;
import org.gitlab.demo.helper.ControllerHelper;
import org.gitlab.demo.helper.IocHelper;
import org.gitlab.demo.util.ClassUtil;

/**
 * Created by ybzhu on 2017/3/6.
 */
public final class HelperLoader {

    public static void init() {
        Class<?>[] classList = {
                ClassHelper.class,
                BeanHelper.class,
                IocHelper.class,
                ControllerHelper.class
        };
        for (Class<?> cls : classList) {
            ClassUtil.loadClass(cls.getName(), false);
        }
    }
}
