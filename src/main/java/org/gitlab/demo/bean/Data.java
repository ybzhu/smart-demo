package org.gitlab.demo.bean;

/**
 * Created by ybzhu on 2017/3/6.
 */
public class Data {
    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
