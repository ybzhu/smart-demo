package org.gitlab.demo.bean;

import org.gitlab.demo.util.CastUtil;

import java.util.Map;

/**
 * Created by ybzhu on 2017/3/6.
 */
public class Param {
    private Map<String, Object> paramMap;

    public Param(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

    public long getLong(String name) {
        return CastUtil.castLong(paramMap.get(name));
    }

    public Map<String, Object> getMap() {
        return paramMap;
    }
}
