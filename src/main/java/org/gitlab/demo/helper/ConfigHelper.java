package org.gitlab.demo.helper;

import org.gitlab.demo.ConfigConstant;
import org.gitlab.demo.util.PropsUtil;

import java.util.Properties;

/**
 * Created by ybzhu on 2017/3/3.
 *
 * 1. 通过这个配置助手类统一读取配置文件的信息，而不是分散在项目各处（要用时再用PropsUtil类去读取）
 */
public final class ConfigHelper {

    private static final Properties CONFIG_FILE = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);

    public static String getJdbcDriver() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.JDBC_DRIVER);
    }

    public static String getJdbcUrl() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.JDBC_URL);
    }

    public static String getJdbcUsername() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.JDBC_USERNAME);
    }

    public static String getJdbcPassword() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.JDBC_PASSWORD);
    }

    public static String getAppBasePackage() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.APP_BASE_PACKAGE);
    }

    public static String getAppJspPath() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.APP_JSP_PATH, "/WEB-INF/view/");
    }

    public static String getAppAssetPath() {
        return PropsUtil.getString(CONFIG_FILE, ConfigConstant.APP_ASSET_PATH, "/asset/");
    }
}
