package org.gitlab.demo.helper;

import org.gitlab.demo.annotation.Inject;
import org.gitlab.demo.util.ArrayUtil;
import org.gitlab.demo.util.CollectionUtil;
import org.gitlab.demo.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 处理框架中的 Inject 注释
 * 类中的属性有 @Inject 注释的，使用 setField() 方法设置属性值
 *
 * Created by ybzhu on 2017/3/6.
 */
public class IocHelper {
    static {
        Map<Class<?>, Object> beanMap = BeanHelper.getBeanMap();
        if (CollectionUtil.isNotEmpty(beanMap)) {
            for (Map.Entry<Class<?>, Object> beanEntry : beanMap.entrySet()) {
                Class<?> beanClass = beanEntry.getKey();
                Object beanInstance = beanEntry.getValue();
                Field[] beanFields = beanClass.getDeclaredFields();
                if (ArrayUtil.isNotEmpty(beanFields)) {
                    for (Field beanField : beanFields) {
                        if (beanField.isAnnotationPresent(Inject.class)) {
                            Class<?> beanFieldClass = beanField.getType();
                            Object beanFieldInstance = beanMap.get(beanFieldClass);
                            if (beanFieldInstance != null) {
                                ReflectionUtil.setField(beanInstance, beanField, beanFieldInstance);
                            }
                        }
                    }
                }
            }
        }
    }
}
